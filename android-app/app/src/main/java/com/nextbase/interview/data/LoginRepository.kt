package com.nextbase.interview.data

import android.util.Log
import com.nextbase.interview.data.model.LoggedInUser
import java.io.IOException

class LoginRepository() {
    companion object {
        private const val TAG = "LoginRepository"
    }

    /**
     * This function is doing some work that may take a long time.
     */
    fun login(username: String, password: String): LoggedInUser {
        doWork(username, password)
        if (username == password || (username == "test@test.com" && password == "test")) {
            return LoggedInUser(username, "Test Test")
        } else {
            throw IOException("LOGIN FAILED")
        }
    }

    fun doWork(username: String, password: String) {
        Log.d(TAG, "doing the login work: $username & $password")
        Thread.sleep(1000)
    }
}