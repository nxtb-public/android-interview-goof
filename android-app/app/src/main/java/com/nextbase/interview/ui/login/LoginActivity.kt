package com.nextbase.interview.ui.login

import android.app.Activity
import android.content.Intent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import com.nextbase.interview.databinding.ActivityLoginBinding

import com.nextbase.interview.R
import com.nextbase.interview.data.LoginRepository
import com.nextbase.interview.ui.PictureActivity

class LoginActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "LoginActivity"
    }

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val username = binding.username
        val password = binding.password
        val login = binding.login
        val loading = binding.loading

        login.setOnClickListener {
            val usernameInput = username.text
            val passwordInput = password.text

            loading.visibility = View.VISIBLE

            val user =
                LoginRepository().login(usernameInput.toString(), passwordInput.toString())


            loading.visibility = View.GONE

            Log.d(TAG, "Got a user: $user")

            this.startActivity(Intent(this, PictureActivity::class.java))
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.d(TAG, "onDestroy called")
    }
}
