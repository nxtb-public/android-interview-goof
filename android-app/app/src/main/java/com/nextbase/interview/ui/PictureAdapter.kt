package com.nextbase.interview.ui

import android.content.Context
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.nextbase.interview.databinding.GridItemPictureBinding

import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.RecyclerView
import com.nextbase.interview.ui.PictureAdapter.PictureViewHolder
import java.net.URL


class PictureAdapter(context: Context, private val pictures: Array<String>) :
    ListAdapter<String, PictureViewHolder>(object : ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }
    }) {

    class PictureViewHolder(val binding: GridItemPictureBinding) :
        RecyclerView.ViewHolder(binding.root)

    companion object {
        const val TAG = "PictureAdapter"
    }


    override fun getItemCount() = pictures.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PictureViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return PictureViewHolder(GridItemPictureBinding.inflate(inflater))
    }

    override fun onBindViewHolder(holder: PictureViewHolder, position: Int) {
        val url = URL("https://picsum.photos/id/${1000 + position}/200/300")
        val bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream())

        holder.binding.image.setImageBitmap(bmp)
    }
}