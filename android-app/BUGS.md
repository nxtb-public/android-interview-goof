1. The contents of the password field are visible
2. When rotating the screen to landscape, the login button is gone
3. When pressing "Sign In Or Register", the App appears completely frozen & after a while it will recover to show the progress bar again 
4. The picture page currently crashes when opened, there is a NetworkOnMainThread exception
5. After a successful sign in, the user can press the back button (on the pictures page) to go back to the Login page.
   Pressing back should close the App.
6. When the user scrolls through the picture list, how can we ensure the images are cached?
7. Not a single test was written to simulate the user input or the form submission. There should be at least one test to verify the user input.
